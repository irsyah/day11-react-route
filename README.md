# SERVER (DEADLINE: Senin, 29 Aug 2022 pukul 19:00)
Buatlah REST API untuk melakukan:
- Registrasi (Create)
- Login (Read)
- Update user_detail (Update) => HOLD (JANGAN DIKERJAKAN DULU)

Tabel users harus memiliki column:
- id: Long
- email (unique): String
- password: String
- role: String

Tabel user_detail memiliki column: => HOLD (JANGAN DIKERJAKAN DULU)
- id: Long
- userId (FK dari users): Long
- address: String
- gender: String
- birthDate: Date
- balance: Big Int


# CLIENT (DEADLINE: Sabtu, 27 Aug 2022 pukul 23:59)
- Buatlah Navbar di mana terdapat menu:
    - Home (menampilkan )
    - Books
    - Profile
    - Cart

- Buatlah route untuk:
    - '/' me-render page Home
    - '/books' me-render page Book
    - '/book/:id' me-render component BookDetail
    - '/book/add' me-render component formBook
    - '/profile' me-render page Profile
    - '/cart' me-render page Cart

- Page Home menampilkan Welcome Banner Book Store App kalian

- Page Book menampilkan kumpulan list book dimana setiap buku memiliki tombol "Detail". Ketika tombol Detail di-click maka diarahkan ke route '/book/:id' (harus fetch data REST API KALIAN YANG MEMANGGIL METHOD findById dari service)

- Pada Page Book terdapat tombol "Add Book" yang akan mengarahkan ke route '/book/add'

- Pada component BookDetail terdapat tombol "Add to Cart" dan jumlah buku yang ingin dibeli

