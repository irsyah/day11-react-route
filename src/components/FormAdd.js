import React from 'react';
import { useNavigate } from 'react-router-dom';

function FormAdd() {
  const navigate = useNavigate();

  const save = (e) => {
    e.preventDefault();
    // setelah berhasil proses save maka redirect
    navigate('/rent');
  } 

  return (
    <div>
        <h1>formAdd</h1>
        <form>
            <button onClick={save}>Submit</button>
        </form>
    </div>
    
  )
}

export default FormAdd