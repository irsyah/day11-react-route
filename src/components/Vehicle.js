import React from 'react';
import { Link } from 'react-router-dom';
import { convertCurrency } from '../util';

function Vehicle({ vehicle, setVehicles, setBalance, balance }) {

  const rentOrReturn = async () => {
    try {
        const response = await fetch(`http://movie-app-g2.herokuapp.com/vehicles/${vehicle._id}`, {
                                        method:'PATCH',
                                        headers: {
                                            'Content-Type': 'application/json'
                                        },
                                        // kalo status false, saya akan ubah menjadi true
                                        // kalo status true, saya akan ubah jadi false
                                        body: JSON.stringify({ status: !vehicle.status })
                                    });
        if (response.ok) {
            setVehicles(currentVehicle => {
                let updated = currentVehicle.map(state => {
                    if (state._id === vehicle._id) {
                        return { ...state, status: !vehicle.status }
                    } 
                    return state;
                })

                return updated;
            })

            if (!vehicle.status) { // kalo mau rent, maka balance berkurang
                setBalance(vehicle.price);
            }
            
        }

    } catch(err) {
        console.log(err);
    }
  }


  return (
    <div className="card col-3 m-3">
        <img className="card-img-top mx-auto d-block mb-3" style={ {width: "350px", height: "350px"}} src={vehicle.image} />
        <h3>{vehicle.type_of_vehicle}</h3>
        <h4 className="mx-auto"> {convertCurrency(vehicle.price, 'en-US', 'USD')} </h4>
        <div className="mt-auto">
            {
                !vehicle.status && balance >= vehicle.price ? 
                <button className="btn btn-primary mx-auto d-block mb-3 mt-3" onClick={rentOrReturn}>Rent</button> 
                : (!vehicle.status && balance < vehicle.price) ?
                <button className="btn btn-primary mx-auto d-block mb-3 mt-3" onClick={rentOrReturn} disabled>Rent</button> 
                : <button className="btn btn-warning mx-auto d-block mb-3 mt-3" onClick={rentOrReturn}>Return</button>
            }
            <Link to={`/vehicle/${vehicle._id}`} className="btn btn-danger mx-auto d-block mb-3 mt-3">Detail</Link> 
        </div>
    </div>
  )
}

export default Vehicle