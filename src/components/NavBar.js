import React from 'react';
import { Link } from 'react-router-dom';

import { convertCurrency } from '../util';

function NavBar({ setMenu, user }) {
  return (
    <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
               <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <Link className="nav-link" to='/'>Home</Link>
                    <Link className="nav-link" to='/rent'>Rent</Link>
                    <Link className="nav-link" to='/about'>About</Link>
                    {/* <div id="home" className="nav-link" onClick={() => setMenu('Home')}>Home</div>
                    <div id="rent" className="nav-link" onClick={() => setMenu('Rent')}>Rent</div> */}
                </div>
                </div>
                <p className='d-flex flex-row-reverse'>Hi {user.user} your balance: {convertCurrency(user.balance, 'en-US', 'USD')}</p>
            </div>
        </nav>
    </div>
  )
}

export default NavBar