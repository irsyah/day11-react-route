import React from 'react'
import { useParams, Outlet, Link, Routes, Route } from 'react-router-dom';
import Info from '../components/Info';
import Specification from '../components/Specification';

function VehicleDetail() {
  const { id } = useParams();

//   fetch data book kalian berdasarkan findById
  return (
    <div>
        <h1>PAGE VEHICLE DETAIL</h1>
        <h3>{id}</h3>

        <div>
            <Link to={`/vehicle/${id}/spec`}>Specification || </Link>
            <Link to={`/vehicle/${id}/info`}>Info</Link>
        </div>

        <Outlet />
    </div>
  )
}





export default VehicleDetail