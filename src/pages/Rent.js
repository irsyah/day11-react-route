import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import Vehicle from '../components/Vehicle';

function Rent({ setUser, balance }) {
  const [ vehicles, setVehicles ] = useState([]);

  const getAllVehicle = async () => {
    try {
        const response = await fetch(`http://movie-app-g2.herokuapp.com/vehicles`);
        if (response.ok) {
            const data = await response.json();
            setVehicles(data);
        } else {
            throw Error(`Error status: ${response.status}`)
        }
    } catch(err) {
        console.log(err);
    }
  }

  const setBalance = (price) => {
    setUser(stateUser => {
        return { ...stateUser, balance: stateUser.balance - price }
    })
  }

  useEffect(() => {
    getAllVehicle();
  }, [])

  return (
    <div>
        <Link to='/add/vehicle' className='btn btn-primary'>Add</Link>
        <div className='row justify-content-center'>
            { vehicles.map(vehicle => (
                <Vehicle 
                    key={vehicle._id} 
                    vehicle={vehicle}
                    setVehicles={setVehicles}
                    setBalance={setBalance}
                    balance={balance}
                />
            ))}
        </div>
    </div>
  )
}

export default Rent