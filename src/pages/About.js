import React from 'react'
import { convertCurrency } from '../util'

function About({ user }) {
  return (
    <div>
        <h1>Username: {user.user}</h1>
        <h3>Balance: {convertCurrency(user.balance, 'en-US', 'USD')}</h3>
    </div>
  )
}

export default About