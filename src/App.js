import { useState } from "react";
import { Routes, Route } from "react-router-dom";
import FormAdd from "./components/FormAdd";
import Info from "./components/Info";

import NavBar from "./components/NavBar";
import Specification from "./components/Specification";
import About from "./pages/About";
import Home from './pages/Home'
import Rent from "./pages/Rent";
import VehicleDetail from "./pages/VehicleDetail";


function App() {
  // const [ menu, setMenu ] = useState('');
  const [ user, setUser ] = useState({
                                        "user" : "icha",
                                        "balance": 5000
                                    })

  return (
    <div >
      {/* url routing : .../rent */}
      <NavBar user={user}/> 
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/rent' element={<Rent setUser={setUser} balance={user.balance}/>}/>
        
        <Route path='/add/vehicle' element={<FormAdd />} />

        {/* CONTOH NESTED ROUTE */}
        <Route path='/vehicle/:id' element={<VehicleDetail />} >
          <Route path='spec' element={<Specification />} />
          <Route path='info' element={<Info />} />
        </Route>
        
        <Route path='/about' element={<About user={user}/>} />
        <Route path='/*' element={<h1>Page Not Found</h1>} />
      </Routes>

      {/* { menu === 'Home' ? <Home /> : menu === 'Rent' ? <Rent setUser={setUser} balance={user.balance}/> : <Home />}     */} 
    </div>
  );
}

export default App;

// repo: book-store-app
// - folder: server dan client

// Buat path '/'
// Buat path '/books'
// Buat path '/profile'
// Buat path '/cart'
